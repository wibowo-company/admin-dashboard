<?php

namespace App\Controllers;

class Home extends BaseController
{

	public function index()
	{

		$session = \Config\Services::session();

		if (!$session->has('user_id')) {

			return redirect()->to('/login?msg=Please Login');
		} else {

			$page = 'Dashboard';

			$meetingModel = new \App\Models\MeetingModel();
			$applicationModel = new \App\Models\ApplicationModel();

			$meetings = $meetingModel->getMeetings(3, 'ASC', date('Y-m-d'));
			$vData['meetings'] = $meetings;

			$applications = $applicationModel->getApplications([], 3);
			$vData['applications'] = $applications;

			$data['pageTitle'] = $page . ' | Wibowo & Company Admin';
			$data['activeNav'] = $page;
			$data['content'] = view('home', $vData);

			return view('base_view', $data);
		}
	}
}
