<?php

namespace App\Controllers;

class Application extends BaseController {

	public function index() {

		$session = \Config\Services::session($config);

		if(!$session->has('user_id')) {
			
			return redirect()->to('/login?msg=Please Login');
		
		} else {	

			$page = 'Application';

            $applicationModel = new \App\Models\ApplicationModel();

			if(isset($_GET['application_type'])){
				if($_GET['application_type'] != 'ALL'){
					$filter['application_type'] = $_GET['application_type'];
				} else {
					$filter = [];
				}
			} else {
				$filter = [];
			}

            $applications = $applicationModel->getApplications($filter);

            if($applications){
                $vData['applications'] =  $applications;
            } else {
                $vData['applications'] = null;
            }
            
			$data['pageTitle'] = $page . ' | Wibowo & Company Admin';
			$data['activeNav'] = $page;
			$data['content'] = view('application', $vData);

			return view('base_view', $data);

		}

	}

}
