<?php

namespace App\Controllers;

class Login extends BaseController {
	
    public function index() {   

        $session = \Config\Services::session();
        $view = \Config\Services::renderer();

		if($session->has('user_id')) {
			
			return redirect()->to('/');
		
		} else {

            $data['pageTitle'] = 'Login | Wibowo & Company Admin';
			
            return view('login', $data);

		}
	
    }

    public function submit() {

        $request = service('request');
        $session = \Config\Services::session();

        if( (!empty($request->getPost('username'))) && (!empty($request->getPost('password'))) ){

            $usersModel = new \App\Models\UsersModel();
            $username = $request->getPost('username');
            $password = $request->getPost('password');

            $userData = $usersModel->userLogin($username, $password);

            if(!$userData){

                return redirect()->to('/login?msg=Invalid Credentials&type=error');

            } else {

                $sessionUserData = [
                    "user_id" => $userData->id,
                    "username" => $userData->username,
                    "fullname" => $userData->fullname,
                    "email" => $userData->email
                ];

                $session->set($sessionUserData);

                return redirect()->to('/?msg=Welcome ' . $userData->fullname . '!');

            }

        } else {

            return redirect()->to('/login?msg=Unauthorized Access');

        }

    }

}
