<?php

namespace App\Controllers;

class MeetingSchedule extends BaseController
{

    public function index()
    {

        $session = \Config\Services::session();

        if (!$session->has('user_id')) {

            return redirect()->to('/login?msg=Please Login');
        } else {

            $meetingScheduleModel = new \App\Models\MeetingScheduleModel();

            if (isset($_GET['action']) && isset($_GET['id'])) {

                if ($_GET['action'] == 'delete') {

                    $meetingScheduleModel->deleteMeetingSchedule($_GET['id']);
                    return redirect()->to('/meeting_schedule?msg=Schedule Deleted Successfully&type=success');
                }
            }

            $page = 'Meeting Schedule';

            if (isset($_GET['date'])) {
                $filter['date'] = $_GET['date'];
            } else {
                $filter = [];
            }

            $schedules = $meetingScheduleModel->getMeetingSchedules($filter);

            if ($schedules) {
                $vData['schedules'] =  $schedules;
            } else {
                $vData['schedules'] = null;
            }

            $data['pageTitle'] = $page . ' | Wibowo & Company Admin';
            $data['activeNav'] = $page;
            $data['content'] = view('meeting_schedule', $vData);

            return view('base_view', $data);
        }
    }

    public function submit()
    {

        if (isset($_POST['date']) && isset($_POST['starttime']) && isset($_POST['endtime'])) {

            $meetingScheduleModel = new \App\Models\MeetingScheduleModel();

            $timeArray = $this->create_time_range($_POST['starttime'], $_POST['endtime']);

            if (count($timeArray) > 0) {

                foreach ($timeArray as $t) {

                    $checkScheduleExist = $meetingScheduleModel->checkScheduleExist($_POST['date'], $t);

                    if (!$checkScheduleExist) {

                        $meetingScheduleModel->createMeetingSchedule($_POST['date'], $t);
                    }
                }
            }

            return redirect()->to('/meeting_schedule?msg=Schedule Created&type=success');
        } else {

            return redirect()->to('/meeting_schedule?msg=Invalid Request&type=error');
        }
    }

    private function create_time_range($start, $end, $interval = '30 mins', $format = '24')
    {
        $startTime = strtotime($start);
        $endTime   = strtotime($end);
        $returnTimeFormat = ($format == '12') ? 'h:i:s A' : 'H:i:s';

        $current   = time();
        $addTime   = strtotime('+' . $interval, $current);
        $diff      = $addTime - $current;

        $times = array();
        while ($startTime < $endTime) {
            $times[] = date($returnTimeFormat, $startTime);
            $startTime += $diff;
        }
        $times[] = date($returnTimeFormat, $startTime);
        return $times;
    }
}
