<?php

namespace App\Controllers;

class Contact extends BaseController {

	public function index() {

		$session = \Config\Services::session();

		if(!$session->has('user_id')) {
			
			return redirect()->to('/login?msg=Please Login');
		
		} else {	

			$page = 'Contact';

            $contactModel = new \App\Models\ContactModel();

            $contacts = $contactModel->getContacts();

            if($contacts){
                $vData['contacts'] =  $contacts;
            } else {
                $vData['contacts'] = null;
            }
            
			$data['pageTitle'] = $page . ' | Wibowo & Company Admin';
			$data['activeNav'] = $page;
			$data['content'] = view('contact', $vData);

			return view('base_view', $data);

		}

	}

}
