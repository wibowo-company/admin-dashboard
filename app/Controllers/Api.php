<?php

namespace App\Controllers;

class Api extends BaseController
{
    public function getVacancies()
    {


        $vacancyModel = new \App\Models\VacancyModel();
        $filter = [];

        if (isset($_GET['department'])) {
            $filter['department'] = $_GET['department'];
        }

        if (isset($_GET['type'])) {
            $filter['type'] = $_GET['type'];
        }

        if (isset($_GET['search'])) {
            $filter['search'] = $_GET['search'];
        }

        $vacanciesData = $vacancyModel->getVacancies($filter);

        if ($vacanciesData) {
            return $this->response->setStatusCode(200)->setJSON($vacanciesData);
        } else {
            return $this->response->setStatusCode(404)->setJSON(array("msg" => "Vacancy not Found"));
        }
    }

    public function getVacancy($id)
    {

        $vacancyModel = new \App\Models\VacancyModel();
        $vacancyData = $vacancyModel->getVacancyDetail($id);

        if ($vacancyData) {
            return $this->response->setStatusCode(200)->setJSON($vacancyData[0]);
        } else {
            return $this->response->setStatusCode(404)->setJSON(array("msg" => "Vacancy Detail not Found"));
        }
    }

    public function postContact()
    {

        $postData = json_decode(trim(file_get_contents('php://input')), true);

        if (isset($postData['role']) && isset($postData['fullname']) && isset($postData['email']) && isset($postData['phone']) && isset($postData['message'])) {

            $contactModel = new \App\Models\ContactModel();
            $contactModel->createContact($postData);
            return $this->response->setStatusCode(200)->setJSON(array("msg" => "Message Sent Successfully!"));
        } else {
            return $this->response->setStatusCode(400)->setJSON(array("msg" => "Invalid Request Body"));
        }
    }

    public function postMeeting()
    {

        $postData = json_decode(trim(file_get_contents('php://input')), true);

        if (isset($postData['date']) && isset($postData['time']) && isset($postData['type']) && isset($postData['fullname']) && isset($postData['email']) && isset($postData['phone']) && isset($postData['company_name']) && isset($postData['company_size']) && isset($postData['industry']) && isset($postData['division']) && isset($postData['position']) && isset($postData['service']) && isset($postData['education']) && isset($postData['applied_position'])) {

            $meetingModel = new \App\Models\MeetingModel();
            $meetingModel->createMeeting($postData);
            return $this->response->setStatusCode(200)->setJSON(array("msg" => "Meeting Sent Successfully! Our staff will be in touch via email."));
        } else {
            return $this->response->setStatusCode(400)->setJSON(array("msg" => "Invalid Request Body"));
        }
    }

    public function postApplication()
    {

        if (isset($_POST['fullname']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['education']) && isset($_POST['vacancy_id']) && isset($_POST['type']) && isset($_FILES['resume_file'])) {

            $applicationModel = new \App\Models\ApplicationModel();

            $applicationId = $this->generateRandomString(20);

            $allowedExtension = ['txt', 'pdf', 'doc', 'docx', 'jpg', 'png'];

            $resumeFile = $this->request->getFile('resume_file');
            $additionalFile = $this->request->getFile('additional_file');

            $resumeFileNewName = null;
            $additionalFileNewName = null;

            if (in_array($resumeFile->guessExtension(), $allowedExtension)) {
                $resumeFileNewName = "resume_" . $applicationId . "." . $resumeFile->guessExtension();
                $resumeFile->move(ROOTPATH . 'public/assets/uploads/', $resumeFileNewName);
            } else {
                return $this->response->setStatusCode(400)->setJSON(array("msg" => "Resume File Type not Allowed (" . $resumeFile->guessExtension() . ")"));
            }

            if ($additionalFile) {
                if (in_array($additionalFile->guessExtension(), $allowedExtension)) {
                    $additionalFileNewName = "additional_" . $applicationId . "." . $additionalFile->guessExtension();
                    $additionalFile->move(ROOTPATH . 'public/assets/uploads/', $additionalFileNewName);
                } else {
                    return $this->response->setStatusCode(400)->setJSON(array("msg" => "Additional File Type not Allowed (" . $additionalFile->guessExtension() . ")"));
                }
            }

            if(isset($_POST['linkedin'])){
                $linkedin = $_POST['linkedin'];
            } else {
                $linkedin = "";
            }

            $applicationData = array(
                "id" => $applicationId,
                "fullname" => $_POST['fullname'],
                "email" => $_POST['email'],
                "phone" => $_POST['phone'],
                "education" => $_POST['education'],
                "vacancy_id" => $_POST['vacancy_id'],
                "resume_file" => $resumeFileNewName,
                "additional_file" => $additionalFileNewName,
                "linkedin" => $linkedin,
                "type" => $_POST['type']
            );

            $applicationModel->createApplication($applicationData);

            return $this->response->setStatusCode(200)->setJSON(array("msg" => "Application Sent Successfully!"));
        } else {
            return $this->response->setStatusCode(400)->setJSON(array("msg" => "Invalid Request Body"));
        }
    }

    public function getMeetingSchedule()
    {

        if (isset($_GET['date'])) {

            $meetingScheduleModal = new \App\Models\MeetingScheduleModel();
            $meetingSchedules = $meetingScheduleModal->getAvailableMeetingSchedule($_GET['date']);

            if ($meetingSchedules) {
                return $this->response->setStatusCode(200)->setJSON($meetingSchedules);
            } else {
                return $this->response->setStatusCode(404)->setJSON(array("msg" => "Schedule not Found"));
            }
        } else {
            return $this->response->setStatusCode(400)->setJSON(array("msg" => "Invalid Request Body"));
        }
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
