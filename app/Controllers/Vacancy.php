<?php

namespace App\Controllers;

class Vacancy extends BaseController
{

	public function index()
	{

		$session = \Config\Services::session();

		if (!$session->has('user_id')) {

			return redirect()->to('/login?msg=Please Login');
		} else {

			$vacancyModel = new \App\Models\VacancyModel();

			if (isset($_GET['action']) && isset($_GET['id'])) {

				if ($_GET['action'] == 'delete') {

					$vacancyModel->deleteVacancy($_GET['id']);
					return redirect()->to('/vacancy?msg=Vacancy Deleted Successfully&type=success');
				}
			}

			$page = 'Vacancy';



			$vacancies = $vacancyModel->getVacanciesData();

			if ($vacancies) {
				$vData['vacancies'] =  $vacancies;
			} else {
				$vData['vacancies'] = null;
			}

			$data['pageTitle'] = $page . ' | Wibowo & Company Admin';
			$data['activeNav'] = $page;
			$data['content'] = view('vacancy', $vData);

			return view('base_view', $data);
		}
	}

	public function update()
	{

		$p = $_POST;

		if (isset($p['title']) && isset($p['department']) && isset($p['type']) && isset($p['active_until']) && isset($p['description']) && isset($p['requirement']) && isset($p['benefit']) && isset($p['vacancy_id_update'])) {


			$vacancyModel = new \App\Models\VacancyModel();
			$vacancyModel->updateVacancy($p);

			return redirect()->to('/vacancy?msg=Vacancy Updated Successfully&type=success');
		} else {

			return redirect()->to('/vacancy');
		}
	}

	public function submit()
	{

		$p = $_POST;

		if (isset($p['title']) && isset($p['department']) && isset($p['type']) && isset($p['active_until']) && isset($p['description']) && isset($p['requirement']) && isset($p['benefit'])) {


			$vacancyModel = new \App\Models\VacancyModel();
			$vacancyModel->createVacancy($p);

			return redirect()->to('/vacancy?msg=Vacancy Created Successfully&type=success');
		} else {

			return redirect()->to('/vacancy');
		}
	}
}
