<?php

namespace App\Controllers;

class Meeting extends BaseController {

	public function index() {

		$session = \Config\Services::session();

		if(!$session->has('user_id')) {
			
			return redirect()->to('/login?msg=Please Login');
		
		} else {	

			$page = 'Meeting';

            $meetingModel = new \App\Models\MeetingModel();

            $meetings = $meetingModel->getMeetings();

            if($meetings){
                $vData['meetings'] =  $meetings;
            } else {
                $vData['meetings'] = null;
            }
            
			$data['pageTitle'] = $page . ' | Wibowo & Company Admin';
			$data['activeNav'] = $page;
			$data['content'] = view('meeting', $vData);

			return view('base_view', $data);

		}

	}

}
