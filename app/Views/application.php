<style>
    .dataTables_filter {
        width: 60%;
        float: right;
        text-align: right;
    }
</style>

<?php

if (isset($_GET['application_type'])) {
    $selectedApplicationType = $_GET['application_type'];
} else {
    $selectedApplicationType = "";
}

?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Application List</h4>
                <p class="card-category">Applications data from website</p>
            </div>
            <div class="card-body">

                <form action="" method="GET">
                    <div class="row mb-5 mt-5">

                        <div class="col-4">
                            <select class="form-control" name="application_type" required>
                                <option value="">Filter Application Type</option>
                                <option value="ALL" <?php if ($selectedApplicationType == 'ALL') {
                                                            echo "selected";
                                                        } ?>>All</option>
                                <option value="office" <?php if ($selectedApplicationType == 'office') {
                                                            echo "selected";
                                                        } ?>>Office</option>
                                <option value="worker" <?php if ($selectedApplicationType == 'worker') {
                                                            echo "selected";
                                                        } ?>>Worker</option>
                            </select>
                        </div>
                        <div class="col-2">
                            <button type="submit" class="btn btn-success">Filter</button>
                        </div>

                    </div>
                </form>

                <table id="datatables" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Education</th>
                            <th>Vacancy / Dept</th>
                            <th>Vacancy Type</th>
                            <th>Office / Worker</th>
                            <th>Resume</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($applications) { ?>
                            <?php foreach ($applications as $p) { ?>

                                <tr>
                                    <td><?= $p->fullname; ?></td>
                                    <td><?= $p->education; ?></td>
                                    <td><?= $p->vacancy_title; ?> / <?= $p->vacancy_department; ?></td>
                                    <td><?= $p->vacancy_type; ?></td>
                                    <td><?= ucwords($p->application_type); ?></td>
                                    <td><a href="<?= base_url('assets/uploads/' . $p->resume_file); ?>" target="_blank">Download</a></td>
                                    <td><a href="#" class="btn btn-info btn-sm" onclick='showDetail(<?= json_encode($p, JSON_UNESCAPED_SLASHES); ?>)'>Detail</a></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="6">No Data Available</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Full Name</th>
                            <th>Education</th>
                            <th>Vacancy / Dept</th>
                            <th>Vacancy Type</th>
                            <th>Office / Worker</th>
                            <th>Resume</th>
                            <th>Detail</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Application Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5">Full Name</div>
                    <div class="col-7" id="modalFullname"></div>
                </div>
                <div class="row">
                    <div class="col-5">Applied Vacancy</div>
                    <div class="col-7" id="modalAppliedVacancy"></div>
                </div>
                <div class="row">
                    <div class="col-5">Vacancy Dept</div>
                    <div class="col-7" id="modalVacancyDept"></div>
                </div>
                <div class="row">
                    <div class="col-5">Vacancy Type</div>
                    <div class="col-7" id="modalVacancyType"></div>
                </div>
                <div class="row">
                    <div class="col-5">Email</div>
                    <div class="col-7" id="modalEmail"></div>
                </div>
                <div class="row">
                    <div class="col-5">Phone</div>
                    <div class="col-7" id="moodalPhone"></div>
                </div>
                <div class="row">
                    <div class="col-5">Education</div>
                    <div class="col-7" id="modalEducation"></div>
                </div>
                <div class="row">
                    <div class="col-5">Resume File</div>
                    <div class="col-7" id="modalResumeFile"></div>
                </div>
                <div class="row">
                    <div class="col-5">Additional File</div>
                    <div class="col-7" id="modalAdditionalFile"></div>
                </div>
                <div class="row">
                    <div class="col-5">LinkedIn</div>
                    <div class="col-7" id="modalLinkedin"></div>
                </div>
                <div class="row">
                    <div class="col-5">Submit Time</div>
                    <div class="col-7" id="modalSubmitTime"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function showDetail(d) {

        document.getElementById('modalFullname').innerHTML = d.fullname;
        document.getElementById('modalAppliedVacancy').innerHTML = d.vacancy_title;
        document.getElementById('modalVacancyDept').innerHTML = d.vacancy_department;
        document.getElementById('modalVacancyType').innerHTML = d.vacancy_type;
        document.getElementById('modalEmail').innerHTML = d.email;
        document.getElementById('moodalPhone').innerHTML = d.phone;
        document.getElementById('modalEducation').innerHTML = d.education;
        document.getElementById('modalLinkedin').innerHTML = d.linkedin;
        document.getElementById('modalResumeFile').innerHTML = `<td><a href="<?= base_url('assets/uploads'); ?>/${d.resume_file}" target="_blank">Download</a></td>`;
        if (d.additional_file) {
            document.getElementById('modalAdditionalFile').innerHTML = `<td><a href="<?= base_url('assets/uploads'); ?>/${d.additional_file}" target="_blank">Download</a></td>`;
        } else {
            document.getElementById('modalAdditionalFile').innerHTML = "-";
        }
        document.getElementById('modalSubmitTime').innerHTML = d.createtime;

        $('#exampleModal').modal('toggle')

    }
</script>