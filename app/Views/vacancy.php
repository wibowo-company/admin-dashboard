<style>
    .dataTables_filter {
        width: 60%;
        float: right;
        text-align: right;
    }

    .modal-body {
        height: 250px;
        overflow-y: auto;
    }

    @media (min-height: 500px) {
        .modal-body {
            height: 400px;
        }
    }

    @media (min-height: 800px) {
        .modal-body {
            height: 600px;
        }
    }
</style>

<div class="row">
    <div class="col-md-12">

        <div class="container-fluid">
            <a href="#" class="btn btn-success btn-md" onclick="onCreateVacancyClick()"><span class="material-icons">add_task</span> Create Vacancy</a>
        </div>

        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Vacancies List</h4>
                <p class="card-category"></p>
            </div>
            <div class="card-body">

                <table id="datatables" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Department</th>
                            <th>Type</th>
                            <th>Active Until</th>
                            <th>Detail</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($vacancies) { ?>
                            <?php foreach ($vacancies as $p) { ?>

                                <tr>
                                    <td><?= $p->title; ?></td>
                                    <td><?= $p->department; ?></td>
                                    <td><?= $p->type; ?></td>
                                    <td><?= $p->active_until; ?></td>
                                    <td><a href="#" class="btn btn-info btn-sm" onclick='showDetail(<?= json_encode($p, JSON_UNESCAPED_SLASHES); ?>)'>Detail</a></td>
                                    <td><a href="#" class="btn btn-warning btn-sm" onclick='updateVacancy(<?= json_encode($p, JSON_UNESCAPED_SLASHES); ?>)'>Edit</a></td>
                                    <td><a href="<?= base_url('vacancy?action=delete&id=' . $p->id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</a></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5">No Data Available</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Department</th>
                            <th>Type</th>
                            <th>Active Until</th>
                            <th>Detail</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Vacancy Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5">Title</div>
                    <div class="col-7" id="modalTitle"></div>
                </div>
                <div class="row">
                    <div class="col-5">Department</div>
                    <div class="col-7" id="modelDepartment"></div>
                </div>
                <div class="row">
                    <div class="col-5">Type</div>
                    <div class="col-7" id="modalType"></div>
                </div>
                <div class="row">
                    <div class="col-5">Active Until</div>
                    <div class="col-7" id="modalActiveUntil"></div>
                </div>
                <div class="row">
                    <div class="col-5">Description</div>
                    <div class="col-7" id="modalDescription"></div>
                </div>
                <div class="row">
                    <div class="col-5">Requirement</div>
                    <div class="col-7" id="modalRequirement"></div>
                </div>
                <div class="row">
                    <div class="col-5">Benefit</div>
                    <div class="col-7" id="modalBenefit"></div>
                </div>
                <div class="row">
                    <div class="col-5">Create Time</div>
                    <div class="col-7" id="modalCreateTime"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createVacancyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Vacancy</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="" method="POST">

                <div class="modal-body">


                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Title</label>
                        <input type="text" class="form-control" name="title" placeholder="" required />
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Department</label>
                        <input type="text" class="form-control" name="department" placeholder="" required />
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Type</label>
                        <select class="form-control" name="type" required>
                            <option value="">Select Type</option>
                            <option value="Full-Time">Full-Time</option>
                            <option value="Internship">Internship</option>
                        </select>
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Active Until</label>
                        <input type="date" class="form-control" name="active_until" placeholder="" required />
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Description</label>
                        <textarea name="description" id="description"></textarea>
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Requirement</label>
                        <textarea name="requirement" id="requirement"></textarea>
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Benefit</label>
                        <textarea name="benefit" id="benefit"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create Vacancy</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="updateVacancyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Vacancy</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="<?= base_url('vacancy/update'); ?>" method="POST">

                <div class="modal-body">


                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Title</label>
                        <input type="text" class="form-control" name="title" id="titleupdate" placeholder="" required />
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Department</label>
                        <input type="text" class="form-control" name="department" id="departmentupdate" placeholder="" required />
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Type</label>
                        <select class="form-control" name="type" id="typeupdate" required>
                            <option value="">Select Type</option>
                            <option value="Full-Time">Full-Time</option>
                            <option value="Internship">Internship</option>
                        </select>
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Active Until</label>
                        <input type="date" class="form-control" name="active_until" id="active_untilupdate" placeholder="" required />
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Description</label>
                        <textarea name="description" id="descriptionupdate"></textarea>
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Requirement</label>
                        <textarea name="requirement" id="requirementupdate"></textarea>
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Benefit</label>
                        <textarea name="benefit" id="benefitupdate"></textarea>
                    </div>

                    <input type="hidden" name="vacancy_id_update" id="vacancy_id_update" required />

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Vacancy</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.7.1/tinymce.min.js" integrity="sha512-RnlQJaTEHoOCt5dUTV0Oi0vOBMI9PjCU7m+VHoJ4xmhuUNcwnB5Iox1es+skLril1C3gHTLbeRepHs1RpSCLoQ==" crossorigin="anonymous"></script>

<script>
    tinymce.init({
        selector: '#description',
        plugins: 'lists advlist',
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
    });
    tinymce.init({
        selector: '#benefit',
        plugins: 'lists advlist',
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
    });
    tinymce.init({
        selector: '#requirement',
        plugins: 'lists advlist',
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
    });

    tinymce.init({
        selector: '#descriptionupdate',
        plugins: 'lists advlist',
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
    });
    tinymce.init({
        selector: '#benefitupdate',
        plugins: 'lists advlist',
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
    });
    tinymce.init({
        selector: '#requirementupdate',
        plugins: 'lists advlist',
        toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
    });
</script>

<script>
    function showDetail(d) {

        document.getElementById('modalTitle').innerHTML = d.title;
        document.getElementById('modelDepartment').innerHTML = d.department;
        document.getElementById('modalType').innerHTML = d.type;
        document.getElementById('modalActiveUntil').innerHTML = d.active_until;
        document.getElementById('modalDescription').innerHTML = d.description;
        document.getElementById('modalRequirement').innerHTML = d.requirement;
        document.getElementById('modalBenefit').innerHTML = d.benefit;
        document.getElementById('modalCreateTime').innerHTML = d.createtime;

        $('#exampleModal').modal('toggle');

    }

    function updateVacancy(d) {

        console.log(d);

        document.getElementById('titleupdate').value = d.title;
        document.getElementById('departmentupdate').value = d.department;
        document.getElementById('typeupdate').value = d.type;
        document.getElementById('active_untilupdate').value = d.active_until;
        document.getElementById('vacancy_id_update').value = d.id;

        tinymce.get('requirementupdate').setContent(d.requirement);
        tinymce.get('benefitupdate').setContent(d.benefit);
        tinymce.get('descriptionupdate').setContent(d.description);

        $('#updateVacancyModal').modal('toggle');

    }

    function onCreateVacancyClick() {
        $('#createVacancyModal').modal('toggle');
    }
</script>