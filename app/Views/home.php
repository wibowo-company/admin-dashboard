<div class="row">
    <div class="col-md-6">

        <div class="card">

            <div class="card-header card-header-primary">
                <h4 class="card-title">Next 3 Meetings</h4>
                <p class="card-category"></p>
            </div>

            <div class="card-body">

                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center"><b>Fullname</b></th>
                            <th class="text-center"><b>Company</b></th>
                            <th class="text-center"><b>Date Time</b></th>
                            <th class="text-center"><b>Detail</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($meetings) { ?>
                            <?php foreach ($meetings as $m) { ?>
                                <tr>

                                    <td class="text-center"><?= $m->fullname; ?></td>
                                    <td class="text-center"><?= $m->company_name; ?></td>
                                    <td class="text-center"><?= $m->date; ?> <?= $m->time; ?></td>
                                    <td class="td-actions text-center">
                                        <a href="#" onclick='showDetail(<?= json_encode($m, JSON_UNESCAPED_SLASHES); ?>)' class="btn btn-info">
                                            <i class="material-icons">visibility</i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>

                <p class="text-right"><a href="<?= base_url('meeting'); ?>">Show All Meetings ></a></p>

            </div>
        </div>
    </div>

    <div class="col-md-6">

        <div class="card">

            <div class="card-header card-header-primary">
                <h4 class="card-title">Last 3 Application</h4>
                <p class="card-category"></p>
            </div>

            <div class="card-body">

                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center"><b>Fullname</b></th>
                            <th class="text-center"><b>Vacancy / Dept</b></th>
                            <th class="text-center"><b>Apply Time</b></th>
                            <th class="text-center"><b>Detail</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($applications) { ?>
                            <?php foreach ($applications as $m) { ?>
                                <tr>

                                    <td class="text-center"><?= $m->fullname; ?></td>
                                    <td class="text-center"><?= $m->vacancy_title; ?> / <?= $m->vacancy_department; ?></td>
                                    <td class="text-center"><?= $m->createtime; ?></td>
                                    <td class="td-actions text-center">
                                        <a href="#" onclick='showAppDetail(<?= json_encode($m, JSON_UNESCAPED_SLASHES); ?>)' class="btn btn-info">
                                            <i class="material-icons">visibility</i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>

                <p class="text-right"><a href="<?= base_url('application'); ?>">Show All Applications ></a></p>

            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Meeting Appointment Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5">Fullname</div>
                    <div class="col-7" id="modalFullname"></div>
                </div>
                <div class="row">
                    <div class="col-5">Phone</div>
                    <div class="col-7" id="modalPhone"></div>
                </div>
                <div class="row">
                    <div class="col-5">Email</div>
                    <div class="col-7" id="modalEmail"></div>
                </div>
                <div class="row">
                    <div class="col-5">Company Name</div>
                    <div class="col-7" id="modalCompanyName"></div>
                </div>
                <div class="row">
                    <div class="col-5">Company Size</div>
                    <div class="col-7" id="modalCompanySize"></div>
                </div>
                <div class="row">
                    <div class="col-5">Industry</div>
                    <div class="col-7" id="modalIndustry"></div>
                </div>
                <div class="row">
                    <div class="col-5">Division</div>
                    <div class="col-7" id="modalDivision"></div>
                </div>
                <div class="row">
                    <div class="col-5">Position</div>
                    <div class="col-7" id="modalPosition"></div>
                </div>
                <div class="row">
                    <div class="col-5">Meeting Type</div>
                    <div class="col-7" id="modalMeetingType"></div>
                </div>
                <div class="row">
                    <div class="col-5">Meeting Date</div>
                    <div class="col-7" id="modalMeetingDate"></div>
                </div>
                <div class="row">
                    <div class="col-5">Meeting Time</div>
                    <div class="col-7" id="modalMeetingTime"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function showDetail(d) {

        document.getElementById('modalFullname').innerHTML = d.fullname;
        document.getElementById('modalPhone').innerHTML = d.phone;
        document.getElementById('modalEmail').innerHTML = d.email;
        document.getElementById('modalCompanyName').innerHTML = d.company_name;
        document.getElementById('modalCompanySize').innerHTML = d.company_size;
        document.getElementById('modalIndustry').innerHTML = d.industry;
        document.getElementById('modalDivision').innerHTML = d.division;
        document.getElementById('modalPosition').innerHTML = d.position;
        document.getElementById('modalMeetingType').innerHTML = d.type;
        document.getElementById('modalMeetingDate').innerHTML = d.date;
        document.getElementById('modalMeetingTime').innerHTML = d.time;

        $('#exampleModal').modal('toggle')

    }
</script>

<div class="modal fade" id="appExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Application Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5">Full Name</div>
                    <div class="col-7" id="appModalFullname"></div>
                </div>
                <div class="row">
                    <div class="col-5">Applied Vacancy</div>
                    <div class="col-7" id="appModalAppliedVacancy"></div>
                </div>
                <div class="row">
                    <div class="col-5">Vacancy Dept</div>
                    <div class="col-7" id="appModalVacancyDept"></div>
                </div>
                <div class="row">
                    <div class="col-5">Vacancy Type</div>
                    <div class="col-7" id="appModalVacancyType"></div>
                </div>
                <div class="row">
                    <div class="col-5">Email</div>
                    <div class="col-7" id="appModalEmail"></div>
                </div>
                <div class="row">
                    <div class="col-5">Phone</div>
                    <div class="col-7" id="appMoodalPhone"></div>
                </div>
                <div class="row">
                    <div class="col-5">Education</div>
                    <div class="col-7" id="appModalEducation"></div>
                </div>
                <div class="row">
                    <div class="col-5">Resume File</div>
                    <div class="col-7" id="appModalResumeFile"></div>
                </div>
                <div class="row">
                    <div class="col-5">Additional File</div>
                    <div class="col-7" id="appModalAdditionalFile"></div>
                </div>
                <div class="row">
                    <div class="col-5">Submit Time</div>
                    <div class="col-7" id="appModalSubmitTime"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function showAppDetail(d) {

        document.getElementById('appModalFullname').innerHTML = d.fullname;
        document.getElementById('appModalAppliedVacancy').innerHTML = d.vacancy_title;
        document.getElementById('appModalVacancyDept').innerHTML = d.vacancy_department;
        document.getElementById('appModalVacancyType').innerHTML = d.vacancy_type;
        document.getElementById('appModalEmail').innerHTML = d.email;
        document.getElementById('appMoodalPhone').innerHTML = d.phone;
        document.getElementById('appModalEducation').innerHTML = d.education;
        document.getElementById('appModalResumeFile').innerHTML = `<td><a href="<?= base_url('assets/uploads'); ?>/${d.resume_file}" target="_blank">Download</a></td>`;
        if (d.additional_file) {
            document.getElementById('appModalAdditionalFile').innerHTML = `<td><a href="<?= base_url('assets/uploads'); ?>/${d.additional_file}" target="_blank">Download</a></td>`;
        } else {
            document.getElementById('appModalAdditionalFile').innerHTML = "-";
        }
        document.getElementById('appModalSubmitTime').innerHTML = d.createtime;

        $('#appExampleModal').modal('toggle')

    }
</script>