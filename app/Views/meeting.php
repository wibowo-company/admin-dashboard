<style>
    .dataTables_filter {
        width: 60%;
        float: right;
        text-align: right;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Meetings List</h4>
                <p class="card-category"></p>
            </div>
            <div class="card-body">

                <table id="datatables" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Fullname</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Company Name</th>
                            <th>Company Size</th>
                            <th>Date / Time</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($meetings) { ?>
                            <?php foreach ($meetings as $p) { ?>

                                <tr>
                                    <td><?= $p->fullname; ?></td>
                                    <td><?= $p->phone; ?></td>
                                    <td><?= $p->email; ?></td>
                                    <td><?= $p->company_name; ?></td>
                                    <td><?= $p->company_size; ?></td>
                                    <td><?= $p->date; ?> / <?= $p->time; ?></td>
                                    <td><a href="#" class="btn btn-info btn-sm" onclick='showDetail(<?= json_encode($p, JSON_UNESCAPED_SLASHES); ?>)'>Detail</a></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5">No Data Available</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Fullname</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Company Name</th>
                            <th>Company Size</th>
                            <th>Date / Time</th>
                            <th>Detail</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Meeting Appointment Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5">Fullname</div>
                    <div class="col-7" id="modalFullname"></div>
                </div>
                <div class="row">
                    <div class="col-5">Phone</div>
                    <div class="col-7" id="modalPhone"></div>
                </div>
                <div class="row">
                    <div class="col-5">Email</div>
                    <div class="col-7" id="modalEmail"></div>
                </div>
                <div class="row">
                    <div class="col-5">Company Name</div>
                    <div class="col-7" id="modalCompanyName"></div>
                </div>
                <div class="row">
                    <div class="col-5">Company Size</div>
                    <div class="col-7" id="modalCompanySize"></div>
                </div>
                <div class="row">
                    <div class="col-5">Industry</div>
                    <div class="col-7" id="modalIndustry"></div>
                </div>
                <div class="row">
                    <div class="col-5">Division</div>
                    <div class="col-7" id="modalDivision"></div>
                </div>
                <div class="row">
                    <div class="col-5">Position</div>
                    <div class="col-7" id="modalPosition"></div>
                </div>
                <div class="row">
                    <div class="col-5">Service Needs</div>
                    <div class="col-7" id="modalServiceNeeds"></div>
                </div>
                <div class="row">
                    <div class="col-5">Meeting Type</div>
                    <div class="col-7" id="modalMeetingType"></div>
                </div>
                <div class="row">
                    <div class="col-5">Meeting Date</div>
                    <div class="col-7" id="modalMeetingDate"></div>
                </div>
                <div class="row">
                    <div class="col-5">Meeting Time</div>
                    <div class="col-7" id="modalMeetingTime"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function showDetail(d) {

        document.getElementById('modalFullname').innerHTML = d.fullname;
        document.getElementById('modalPhone').innerHTML = d.phone;
        document.getElementById('modalEmail').innerHTML = d.email;
        document.getElementById('modalCompanyName').innerHTML = d.company_name;
        document.getElementById('modalCompanySize').innerHTML = d.company_size;
        document.getElementById('modalIndustry').innerHTML = d.industry;
        document.getElementById('modalDivision').innerHTML = d.division;
        document.getElementById('modalPosition').innerHTML = d.position;
        document.getElementById('modalMeetingType').innerHTML = d.type;
        document.getElementById('modalMeetingDate').innerHTML = d.date;
        document.getElementById('modalMeetingTime').innerHTML = d.time;
        document.getElementById('modalServiceNeeds').innerHTML = d.service;

        $('#exampleModal').modal('toggle')

    }
</script>