<style>
    .dataTables_filter {
        width: 60%;
        float: right;
        text-align: right;
    }
</style>

<?php

    if(isset($_GET['date'])){
        $dateValue = $_GET['date'];
    } else {
        $dateValue = "";
    }

?>

<div class="row">
    <div class="col-md-12">

        <div class="container-fluid">
            <a href="#" class="btn btn-success btn-md" onclick="onCreateScheduleClick()"><span class="material-icons">add_task</span> Create Schedule</a>
        </div>

        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Meeting Schedule</h4>
                <p class="card-category"></p>
            </div>
            <div class="card-body">

                <form action="" method="GET">
                    <div class="row mb-5 mt-5">

                        <div class="col-4">
                            <label>Filter by Date</label>
                            <input type="date" class="form-control" name="date" required value="<?=$dateValue;?>" />
                        </div>
                        <div class="col-2 mt-3">
                            <button type="submit" class="btn btn-success">Filter</button>
                            <a href="<?=base_url('meeting_schedule');?>" class="btn btn-warning">Reset</a>
                        </div>

                    </div>
                </form>

                <table id="datatables" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Status</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($schedules) { ?>
                            <?php foreach ($schedules as $p) { ?>

                                <tr>
                                    <td><?= $p->date; ?></td>
                                    <td><?= $p->time; ?></td>
                                    <td><?= (($p->is_booked == 't') ? 'Booked' : 'Available') ?></td>
                                    <td><a href="<?= base_url('meeting_schedule?action=delete&id=' . $p->id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</a></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="6">No Data Available</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Status</th>
                            <th>Delete</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createScheduleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="" method="POST">

                <div class="modal-body">

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Select Date</label>
                        <input type="date" class="form-control" name="date" placeholder="" required />
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Select Start Time</label>
                        <input type="time" class="form-control" name="starttime" placeholder="" required />
                    </div>

                    <div class="form-group mt-5">
                        <label for="exampleFormControlSelect1">Select End Time</label>
                        <input type="time" class="form-control" name="endtime" placeholder="" required />
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create Schedule</button>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="createScheduleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Meeting Appointment Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function onCreateScheduleClick() {
        $('#createScheduleModal').modal('toggle');
    }
</script>