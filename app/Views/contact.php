<style>
    .dataTables_filter {
        width: 60%;
        float: right;
        text-align: right;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Contact List</h4>
                <p class="card-category">Data Sent From Contact Form on Website</p>
            </div>
            <div class="card-body">

                <table id="datatables" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Fullname</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Submit Time</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($contacts) { ?>
                            <?php foreach ($contacts as $p) { ?>

                                <tr>
                                    <td><?= $p->fullname; ?></td>
                                    <td><?= $p->phone; ?></td>
                                    <td><?= $p->email; ?></td>
                                    <td><?= $p->role; ?></td>
                                    <td><?= $p->createtime; ?></td>
                                    <td><a href="#" class="btn btn-info btn-sm" onclick='showDetail(<?= json_encode($p, JSON_UNESCAPED_SLASHES); ?>)'>Show</a></td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="4">No Data Available</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>    
                            <th>Fullname</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Submit Time</th>
                            <th>Message</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Contact Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12" id="modalMessage"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function showDetail(d) {

        document.getElementById('modalMessage').innerHTML = d.message;

        $('#exampleModal').modal('toggle')

    }
</script>