<?php

namespace App\Models;

use CodeIgniter\Model;

class ContactModel extends Model
{

    public function createContact($d)
    {
        $db = \Config\Database::connect();
        $createContactQuery = "INSERT INTO contact(id, role, fullname, email, phone, message, createtime) VALUES(:id:, :role:, :fullname:, :email:, :phone:, :message:, NOW())";
        $createContactExec = $db->query($createContactQuery, [
            "id" => $this->generateRandomString(20),
            "role" => $d['role'],
            "fullname" => $d['fullname'],
            "email" => $d['email'],
            "phone" => $d['phone'],
            "message" => $d['message'],
        ]);

        return true;
    }

    public function getContacts()
    {

        $db = \Config\Database::connect();
        $query = $db->query("SELECT * FROM contact A ORDER BY A.createtime DESC");

        $result = $query->getResult();

        if (count($result) > 0) {

            return $result;
        } else {

            return false;
        }
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
}
