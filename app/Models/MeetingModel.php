<?php

namespace App\Models;

use CodeIgniter\Model;

class MeetingModel extends Model
{

    public function createMeeting($d)
    {
        $db = \Config\Database::connect();

        $meetingIds = implode($d['time'], "','");

        $getMeetingsTime = $db->query("SELECT time FROM meeting_schedule WHERE id IN ('" . $meetingIds . "') ORDER BY time ASC");

        $meetingsTime = $getMeetingsTime->getResult();

        $meetingStartTime = $meetingsTime[0]->time;
        $meetingEndTime = $meetingsTime[count($meetingsTime) - 1]->time;

        $meetingTime = $meetingStartTime . " - " . $meetingEndTime;

        $meetingDuration = round(abs(strtotime($meetingEndTime) - strtotime($meetingStartTime)) / 60, 2);

        if($meetingDuration < 1){
            $meetingDuration = 30;
        }

        $createMeetingQuery = "INSERT INTO meeting(id, date, time, type, duration, fullname, email, phone, company_name, company_size, industry, division, position, service, education, applied_position, createtime) VALUES(:id:, :date:, :time:, :type:, :duration:, :fullname:, :email:, :phone:, :company_name:, :company_size:, :industry:, :division:, :position:, :service:, :education:, :applied_position:, NOW())";
        $createMeetingExec = $db->query($createMeetingQuery, [
            "id" => $this->generateRandomString(20),
            "date" => $d['date'],
            "time" => $meetingTime,
            "type" => $d['type'],
            "duration" => $meetingDuration,
            "fullname" => $d['fullname'],
            "email" => $d['email'],
            "phone" => $d['phone'],
            "company_name" => $d['company_name'],
            "company_size" => $d['company_size'],
            "industry" => $d['industry'],
            "division" => $d['division'],
            "position" => $d['position'],
            "service" => $d['service'],
            "education" => $d['education'],
            "applied_position" => $d['applied_position']
        ]);

        foreach ($d['time'] as $t) {
            $db->query("UPDATE meeting_schedule SET is_booked = true WHERE id = '" . $t . "'");
        }

        return true;
    }

    public function getMeetings($limit = null, $order = 'DESC', $startdate = '1990-01-01')
    {

        $db = \Config\Database::connect();

        if ($limit != null) {
            $limit = " LIMIT " . $limit;
        } else {
            $limit = "";
        }

        $query = $db->query("SELECT * FROM meeting A WHERE A.date > '" . $startdate . "' ORDER BY A.date " . $order . ", A.time " . $order . " " . $limit);

        $result = $query->getResult();

        if (count($result) > 0) {

            return $result;
        } else {

            return false;
        }
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
