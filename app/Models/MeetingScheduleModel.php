<?php

namespace App\Models;

use CodeIgniter\Model;

class MeetingScheduleModel extends Model
{

    public function createMeetingSchedule($date, $time)
    {
        $db = \Config\Database::connect();
        $createMeetingQuery = "INSERT INTO meeting_schedule(id, date, time, is_booked) VALUES(:id:, :date:, :time:, false)";
        $createMeetingExec = $db->query($createMeetingQuery, [
            "id" => $this->generateRandomString(20),
            "date" => $date,
            "time" => $time
        ]);

        return true;
    }

    public function getMeetingSchedules($filter)
    {

        $db = \Config\Database::connect();

        if (isset($filter['date'])) {
            $where = " WHERE A.date = '" . $filter['date'] . "' ";
        } else {
            $where = "";
        }

        $query = $db->query("SELECT * FROM meeting_schedule A " . $where);

        $result = $query->getResult();

        if (count($result) > 0) {

            return $result;
        } else {

            return false;
        }
    }

    public function deleteMeetingSchedule($id)
    {

        $db = \Config\Database::connect();
        $createMeetingQuery = "DELETE FROM meeting_schedule WHERE id = :id:";
        $createMeetingExec = $db->query($createMeetingQuery, [
            "id" => $id
        ]);

        return true;
    }

    public function getAvailableMeetingSchedule($date)
    {
        $db = \Config\Database::connect();
        $createMeetingQuery = "SELECT id, time FROM meeting_schedule WHERE date = :date: AND is_booked = false";
        $createMeetingExec = $db->query($createMeetingQuery, [
            "date" => $date
        ]);

        $result = $createMeetingExec->getResult();

        if (count($result) > 0) {

            return $result;
        } else {

            return false;
        }
    }

    public function checkScheduleExist($date, $time)
    {
        $db = \Config\Database::connect();
        $checkScheduleQuery = "SELECT * FROM meeting_schedule WHERE date = :date: AND time = :time:";
        $checkScheduleQueryExec = $db->query($checkScheduleQuery, [
            "date" => $date,
            "time" => $time
        ]);

        $result = $checkScheduleQueryExec->getResult();

        if (count($result) > 0) {

            return $result;
        } else {

            return false;
        }
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
