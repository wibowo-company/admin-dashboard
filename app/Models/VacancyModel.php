<?php

namespace App\Models;

use CodeIgniter\Model;

class VacancyModel extends Model
{

    public function getVacancies($filter)
    {

        $db = \Config\Database::connect();

        $where = "WHERE is_active = true AND active_until >= CURRENT_DATE AND is_hardcoded = false";

        if (isset($filter['department'])) {
            $where .= " AND LOWER(department) = LOWER('" . $filter['department'] . "')";
        }

        if (isset($filter['type'])) {
            $where .= " AND LOWER(type) = LOWER('" . $filter['type'] . "')";
        }

        if (isset($filter['search'])) {
            $where .= " AND title ILIKE '%" . $filter['search'] . "%'";
        }

        $getVacanciesQuery = "SELECT * FROM vacancy " . $where;

        log_message("info", $getVacanciesQuery);

        $getVacanciesExec = $db->query($getVacanciesQuery);

        $getVacanciesResult = $getVacanciesExec->getResult();

        if (count($getVacanciesResult) > 0) {

            return $getVacanciesResult;
        } else {

            return false;
        }

        return true;
    }

    public function getVacancyDetail($id)
    {

        $db = \Config\Database::connect();

        $getVacanciesQuery = "SELECT * FROM vacancy A JOIN vacancy_detail B ON B.vacancy_id = A.id WHERE A.id = '" . $id . "'";

        log_message("info", $getVacanciesQuery);

        $getVacanciesExec = $db->query($getVacanciesQuery);

        $getVacanciesResult = $getVacanciesExec->getResult();

        if (count($getVacanciesResult) > 0) {

            return $getVacanciesResult;
        } else {

            return false;
        }

        return true;
    }

    public function getVacanciesData()
    {

        $db = \Config\Database::connect();
        $query = $db->query("SELECT A.id, A.title, A.department, A.type, A.active_until, A.createtime, B.description, B.requirement, B.benefit FROM vacancy A JOIN vacancy_detail B ON B.vacancy_id = A.id WHERE A.is_active = true AND is_hardcoded = false ORDER BY A.createtime DESC");

        $result = $query->getResult();

        if (count($result) > 0) {

            return $result;
        } else {

            return false;
        }
    }

    public function deleteVacancy($id)
    {

        $db = \Config\Database::connect();
        $deleteVacancyQuery = "UPDATE vacancy SET is_active = false WHERE id = :id:";
        $deleteVacancyExec = $db->query($deleteVacancyQuery, [
            "id" => $id,
        ]);

        return true;
    }

    public function createVacancy($d)
    {

        $session = \Config\Services::session();

        $vacancyId = $this->generateRandomString(20);
        $vacancyDetailId = $this->generateRandomString(20);

        $db = \Config\Database::connect();
        $createVacancyQuery = "INSERT INTO vacancy(id, title, department, type, active_until, createby, createtime, is_active) VALUES(:id:, :title:, :department:, :type:, :active_until:, :createby:, NOW(), true)";
        $createVacancyExec = $db->query($createVacancyQuery, [
            "id" => $vacancyId,
            "title" => $d['title'],
            "department" => $d['department'],
            "type" => $d['type'],
            "active_until" => $d['active_until'],
            "createby" => $session->fullname
        ]);

        $createVacancyDetailQuery = "INSERT INTO vacancy_detail(id, vacancy_id, description, requirement, benefit) VALUES(:id:, :vacancy_id:, :description:, :requirement:, :benefit:)";
        $createVacancyDetailExec = $db->query($createVacancyDetailQuery, [
            "id" => $vacancyDetailId,
            "vacancy_id" => $vacancyId,
            "description" => $db->escapeString($d['description']),
            "requirement" => $db->escapeString($d['requirement']),
            "benefit" => $db->escapeString($d['benefit'])
        ]);

        return true;
    }

    public function updateVacancy($d)
    {

        $session = \Config\Services::session();

        $db = \Config\Database::connect();
        $createVacancyQuery = "UPDATE vacancy SET title = :title:, department = :department:, type = :type:, active_until = :active_until: WHERE id = :vacancy_id:";
        $createVacancyExec = $db->query($createVacancyQuery, [
            "title" => $d['title'],
            "department" => $d['department'],
            "type" => $d['type'],
            "active_until" => $d['active_until'],
            "vacancy_id" => $d['vacancy_id_update']
        ]);

        $createVacancyDetailQuery = "UPDATE vacancy_detail SET description = :description:, requirement = :requirement:, benefit = :benefit: WHERE vacancy_id = :vacancy_id:";
        $createVacancyDetailExec = $db->query($createVacancyDetailQuery, [
            "description" => $db->escapeString($d['description']),
            "requirement" => $db->escapeString($d['requirement']),
            "benefit" => $db->escapeString($d['benefit']),
            "vacancy_id" => $d['vacancy_id_update']
        ]);

        return true;
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
