<?php

namespace App\Models;

use CodeIgniter\Model;

class ApplicationModel extends Model
{

    public function createApplication($d)
    {
        $db = \Config\Database::connect();
        $createApplicationQuery = "INSERT INTO application(id, fullname, email, phone, vacancy_id, education, resume_file, additional_file, createtime, type, linkedin) VALUES(:id:, :fullname:, :email:, :phone:, :vacancy_id:, :education:, :resume_file:, :additional_file:, NOW(), :type:, :linkedin:)";
        $createApplicationExec = $db->query($createApplicationQuery, [
            "id" => $d['id'],
            "fullname" => $d['fullname'],
            "email" => $d['email'],
            "phone" => $d['phone'],
            "vacancy_id" => $d['vacancy_id'],
            "education" => $d['education'],
            "resume_file" => $d['resume_file'],
            "additional_file" => $d['additional_file'],
            "type" => $d['type'],
            "linkedin" => $d['linkedin']
        ]);

        return true;
    }

    public function getApplications($filter, $limit = null)
    {

        $db = \Config\Database::connect();

        if (isset($filter['application_type'])) {
            $where = " WHERE A.type = '" . $filter['application_type'] . "' ";
        } else {
            $where = "";
        }

        if ($limit != null) {
            $limit = " LIMIT " . $limit . " ";
        } else {
            $limit = "";
        }

        $query = $db->query("SELECT A.id, A.fullname, A.email, A.phone, A.education, A.resume_file, A.additional_file, A.createtime, B.title AS vacancy_title, B.department AS vacancy_department, B.type AS vacancy_type, A.type AS application_type, A.linkedin FROM application A JOIN vacancy B ON A.vacancy_id = B.id " . $where . " ORDER BY A.createtime DESC " . $limit);

        $result = $query->getResult();

        if (count($result) > 0) {

            return $result;
        } else {

            return false;
        }
    }
}
