FROM php:7.3-apache
MAINTAINER Aji Prakoso <ajiprakoso1995@gmail.com>

RUN apt-get update
RUN apt-get install -y libpq-dev zip nano zlib1g-dev libzip-dev libicu-dev

RUN docker-php-ext-install zip intl pdo pdo_pgsql pgsql

RUN rm -Rf /var/www/html

COPY app /var/www/app
COPY public /var/www/html
COPY system /var/www/system
COPY writable /var/www/writable
COPY .env /var/www/.env

COPY custom.ini /usr/local/etc/php/conf.d/custom.ini

ADD start.sh /bootstrap/start.sh

RUN chmod 755 /bootstrap/start.sh

WORKDIR /var/www

RUN chown -R www-data:www-data /var/www

RUN chmod -R 755 app
RUN chmod -R 755 html
RUN chmod -R 755 system
RUN chmod -R 755 writable

RUN a2enmod rewrite

ENTRYPOINT ["/bootstrap/start.sh"]
